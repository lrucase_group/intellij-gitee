// Copyright 2000-2020 JetBrains s.r.o. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
package com.gitee.extensions

import com.gitee.authentication.accounts.GEAccountManager
import com.gitee.authentication.accounts.GiteeAccount
import com.intellij.collaboration.util.serviceGet
import com.intellij.openapi.components.Service
import com.intellij.platform.util.coroutines.childScope
import git4idea.remote.hosting.http.HostedGitAuthenticationFailureManager
import kotlinx.coroutines.CoroutineScope

@Service(Service.Level.PROJECT)
internal class GEGitAuthenticationFailureManager(parentsCs: CoroutineScope)
  :HostedGitAuthenticationFailureManager<GiteeAccount>(serviceGet<GEAccountManager>(), parentsCs.childScope())